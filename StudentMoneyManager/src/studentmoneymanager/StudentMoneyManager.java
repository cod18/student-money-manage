package studentmoneymanager;

/**
 *
 * @author KrismaAditya
 */
public class StudentMoneyManager{
    
    public static void main(String[] args) {
        sql sql = new sql();
        
        sql.createNewDatabase("studentmoneymanager.db");
        sql.createNewTable();
        
        welcomepage welcomepage = new welcomepage();
        welcomepage.setVisible(true);
        
        StudentMoneyManagerGUI gui = new StudentMoneyManagerGUI();
        gui.setVisible(true);
        
        sql.selectAll();
    }   
}