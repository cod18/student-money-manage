package studentmoneymanager;

import java.sql.*;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author KrismaAditya
 */
public class StudentMoneyManagerGUI extends javax.swing.JFrame {
    sql koneksi = new sql();
    
    int saldo;
    
    public void selectPemasukan()
    {
        String sql = "SELECT id_pemasukan, nama_pemasukan, nominal_pemasukan FROM pemasukan";
        
        try (Connection conn = koneksi.connect();
                Statement stmt = conn.createStatement();
                ResultSet rs = stmt.executeQuery(sql))
        {
            while(tabelRincianPemasukan.getRowCount() > 0)
            {
                ((DefaultTableModel) tabelRincianPemasukan.getModel()).removeRow(0);
            }
            
            while (rs.next())
            {
                ResultSetMetaData md = rs.getMetaData();
                int columns = md.getColumnCount();
                
                Object[] row = new Object[columns];
                for (int i = 1; i <= columns; i++)
                {  
                    row[i - 1] = rs.getObject(i);
                    System.out.println(rs.getObject(i));
                }
                ((DefaultTableModel) tabelRincianPemasukan.getModel()).insertRow(rs.getRow()-1,row);
                
                //ini buat set Label pemasukan yang diambil dari database
                //labelRincianPemasukan.setText(rs.getObject(columns).toString());
            }
        }
        catch (SQLException e)
        {
            System.out.println(e.getMessage());
        }
    }
    
    public void selectTotalPemasukan()
    {
        String sql = "SELECT SUM(nominal_pemasukan) FROM pemasukan";
        
        try (Connection conn = koneksi.connect();
                Statement stmt = conn.createStatement();
                ResultSet rs = stmt.executeQuery(sql))
        {
            while (rs.next())
            {
                ResultSetMetaData md = rs.getMetaData();
                int columns = md.getColumnCount();
                
                Object[] row = new Object[columns];
                for (int i = 1; i <= columns; i++)
                {  
                    row[i - 1] = rs.getObject(i);
                    System.out.println(rs.getObject(i));
                }
                
                labelNominalTotalPemasukan.setText(rs.getObject(columns).toString());
            }
        }
        catch (SQLException e)
        {
            System.out.println(e.getMessage());
        }
        catch (NullPointerException npe)
        {
            labelNominalTotalPemasukan.setText("0");
        }
    }
    
    public void selectPengeluaran()
    {
        String sql = "SELECT id_pengeluaran, nama_pengeluaran, nominal_pengeluaran FROM pengeluaran";
        
        try (Connection conn = koneksi.connect();
                Statement stmt = conn.createStatement();
                ResultSet rs = stmt.executeQuery(sql))
        {
            while(tabelRincianPengeluaran.getRowCount() > 0)
            {
                ((DefaultTableModel) tabelRincianPengeluaran.getModel()).removeRow(0);
            }
            
            while (rs.next())
            {
                ResultSetMetaData md = rs.getMetaData();
                int columns = md.getColumnCount();
                
                Object[] row = new Object[columns];
                for (int i = 1; i <= columns; i++)
                {  
                    row[i - 1] = rs.getObject(i);
                    System.out.println(rs.getObject(i));
                }
                ((DefaultTableModel) tabelRincianPengeluaran.getModel()).insertRow(rs.getRow()-1,row);
            }
        }
        catch (SQLException e)
        {
            System.out.println(e.getMessage());
        }
    }
    
    public void selectTotalPengeluaran()
    {
        String sql = "SELECT SUM(nominal_pengeluaran) FROM pengeluaran";
        
        try (Connection conn = koneksi.connect();
                Statement stmt = conn.createStatement();
                ResultSet rs = stmt.executeQuery(sql))
        {
            while (rs.next())
            {
                ResultSetMetaData md = rs.getMetaData();
                int columns = md.getColumnCount();
                
                Object[] row = new Object[columns];
                for (int i = 1; i <= columns; i++)
                {  
                    row[i - 1] = rs.getObject(i);
                    System.out.println(rs.getObject(i));
                }
                
                labelNominalTotalPengeluaran.setText(rs.getObject(columns).toString());
            }
        }
        catch (SQLException e)
        {
            System.out.println(e.getMessage());
        }
        catch (NullPointerException npe)
        {
            labelNominalTotalPengeluaran.setText("0");
        }
    }
    
    public void selectSaldo()
    {
        int total_pemasukan = Integer.parseInt(labelNominalTotalPemasukan.getText());
        int total_pengeluaran = Integer.parseInt(labelNominalTotalPengeluaran.getText());
        
        int saldo = total_pemasukan - total_pengeluaran;
        
        //System.out.println("Saldo = "+saldo);
        labelSaldo.setText("Rp "+saldo);
    }
    
    //fungsi select semua user
    public void selectUser()
    {
        String sql = "SELECT id_user, username FROM user";
        
        try (Connection conn = koneksi.connect();
             Statement stmt  = conn.createStatement();
             ResultSet rs    = stmt.executeQuery(sql))
        {
            while(tableUser.getRowCount() > 0)
            {
                ((DefaultTableModel) tableUser.getModel()).removeRow(0);
            }
            while (rs.next())
            {
                ResultSetMetaData md = rs.getMetaData();
                int columns = md.getColumnCount();
                
                Object[] row = new Object[columns];
                for (int i = 1; i <= columns; i++)
                {  
                    row[i - 1] = rs.getObject(i);
                    System.out.println(rs.getObject(i));
                }
                ((DefaultTableModel) tableUser.getModel()).insertRow(rs.getRow()-1,row);
                
                //ini buat set Label username yang diambil dari database
                usernameLabel.setText(rs.getObject(columns).toString());
                
                //System.out.println(rs.getInt("id_user") + rs.getString("username"));
            }
        }
        catch (SQLException e)
        {
            System.out.println(e.getMessage());
        }
    }
    
    /**
     * Creates new form StudentMoneyManagerGUI
     */
    public StudentMoneyManagerGUI() {
        initComponents();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jTabbedPane1 = new javax.swing.JTabbedPane();
        panelPemasukan = new javax.swing.JPanel();
        labelNamaPemasukan = new javax.swing.JLabel();
        inputNamaPemasukan = new javax.swing.JTextField();
        labelNominalPemasukan = new javax.swing.JLabel();
        inputNominalPemasukan = new javax.swing.JTextField();
        tambahPemasukan = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        tabelRincianPemasukan = new javax.swing.JTable();
        hapusPemasukan = new javax.swing.JButton();
        labelNominalTotalPemasukan = new javax.swing.JLabel();
        labelTotalPemasukan = new javax.swing.JLabel();
        panelPengeluaran = new javax.swing.JPanel();
        labelNamaPengeluaran = new javax.swing.JLabel();
        labelNominalPengeluaran = new javax.swing.JLabel();
        inputNamaPengeluaran = new javax.swing.JTextField();
        inputNominalPengeluaran = new javax.swing.JTextField();
        jScrollPane3 = new javax.swing.JScrollPane();
        tabelRincianPengeluaran = new javax.swing.JTable();
        tambahPengeluaran = new javax.swing.JButton();
        hapusPengeluaran = new javax.swing.JButton();
        labelTotalPengeluaran = new javax.swing.JLabel();
        labelNominalTotalPengeluaran = new javax.swing.JLabel();
        panelReport = new javax.swing.JPanel();
        labelSaldoAndaSaatIni = new javax.swing.JLabel();
        labelSaldo = new javax.swing.JLabel();
        jPanel4 = new javax.swing.JPanel();
        generateListUser = new javax.swing.JButton();
        deleteUser = new javax.swing.JButton();
        jScrollPane2 = new javax.swing.JScrollPane();
        tableUser = new javax.swing.JTable();
        welcomeLabel = new javax.swing.JLabel();
        usernameLabel = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Student Money Manager");
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowOpened(java.awt.event.WindowEvent evt) {
                formWindowOpened(evt);
            }
        });

        labelNamaPemasukan.setText("Nama Pemasukan");

        labelNominalPemasukan.setText("Nominal");

        tambahPemasukan.setText("Tambah Pemasukan");
        tambahPemasukan.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tambahPemasukanActionPerformed(evt);
            }
        });

        tabelRincianPemasukan.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null}
            },
            new String [] {
                "id_pemasukan", "Nama Pemasukan", "Nominal", "id_user"
            }
        ));
        jScrollPane1.setViewportView(tabelRincianPemasukan);

        hapusPemasukan.setText("Hapus");
        hapusPemasukan.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                hapusPemasukanActionPerformed(evt);
            }
        });

        labelNominalTotalPemasukan.setFont(new java.awt.Font("Calibri", 1, 18)); // NOI18N
        labelNominalTotalPemasukan.setText("labelTotalPemasukan");

        labelTotalPemasukan.setFont(new java.awt.Font("Calibri", 1, 18)); // NOI18N
        labelTotalPemasukan.setText("Total Pemasukan Anda saat ini : Rp.");

        javax.swing.GroupLayout panelPemasukanLayout = new javax.swing.GroupLayout(panelPemasukan);
        panelPemasukan.setLayout(panelPemasukanLayout);
        panelPemasukanLayout.setHorizontalGroup(
            panelPemasukanLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelPemasukanLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelPemasukanLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelPemasukanLayout.createSequentialGroup()
                        .addGroup(panelPemasukanLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(tambahPemasukan, javax.swing.GroupLayout.PREFERRED_SIZE, 153, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(panelPemasukanLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(panelPemasukanLayout.createSequentialGroup()
                                    .addComponent(labelNamaPemasukan, javax.swing.GroupLayout.PREFERRED_SIZE, 131, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGap(18, 18, 18)
                                    .addComponent(inputNamaPemasukan, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGroup(panelPemasukanLayout.createSequentialGroup()
                                    .addComponent(labelNominalPemasukan)
                                    .addGap(18, 18, 18)
                                    .addComponent(inputNominalPemasukan, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(panelPemasukanLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(panelPemasukanLayout.createSequentialGroup()
                                .addGap(0, 0, Short.MAX_VALUE)
                                .addComponent(labelTotalPemasukan, javax.swing.GroupLayout.PREFERRED_SIZE, 282, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(labelNominalTotalPemasukan, javax.swing.GroupLayout.PREFERRED_SIZE, 177, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 503, Short.MAX_VALUE)))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelPemasukanLayout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(hapusPemasukan, javax.swing.GroupLayout.PREFERRED_SIZE, 87, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );

        panelPemasukanLayout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {inputNamaPemasukan, inputNominalPemasukan, labelNamaPemasukan, labelNominalPemasukan});

        panelPemasukanLayout.setVerticalGroup(
            panelPemasukanLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelPemasukanLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelPemasukanLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelPemasukanLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(labelNamaPemasukan, javax.swing.GroupLayout.PREFERRED_SIZE, 36, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(inputNamaPemasukan, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelPemasukanLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(labelNominalTotalPemasukan, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(labelTotalPemasukan)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(panelPemasukanLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelPemasukanLayout.createSequentialGroup()
                        .addGroup(panelPemasukanLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(labelNominalPemasukan)
                            .addComponent(inputNominalPemasukan, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(41, 41, 41)
                        .addComponent(tambahPemasukan, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 208, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(hapusPemasukan, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(160, Short.MAX_VALUE))
        );

        panelPemasukanLayout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {inputNamaPemasukan, inputNominalPemasukan, labelNamaPemasukan, labelNominalPemasukan, tambahPemasukan});

        panelPemasukanLayout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {labelNominalTotalPemasukan, labelTotalPemasukan});

        jTabbedPane1.addTab("Cash - In", panelPemasukan);

        labelNamaPengeluaran.setText("Nama Pengeluaran");

        labelNominalPengeluaran.setText("Nominal");

        tabelRincianPengeluaran.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null}
            },
            new String [] {
                "id_pengeluaran", "nama_pengeluaran", "nominal_pengeluaran", "id_user"
            }
        ));
        jScrollPane3.setViewportView(tabelRincianPengeluaran);

        tambahPengeluaran.setText("Tambah Pengeluaran");
        tambahPengeluaran.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tambahPengeluaranActionPerformed(evt);
            }
        });

        hapusPengeluaran.setText("Hapus");
        hapusPengeluaran.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                hapusPengeluaranActionPerformed(evt);
            }
        });

        labelTotalPengeluaran.setFont(new java.awt.Font("Calibri", 1, 18)); // NOI18N
        labelTotalPengeluaran.setText("Total Pengeluaran Anda saat ini Rp.");

        labelNominalTotalPengeluaran.setFont(new java.awt.Font("Calibri", 1, 18)); // NOI18N
        labelNominalTotalPengeluaran.setText("totalPengeluaran");

        javax.swing.GroupLayout panelPengeluaranLayout = new javax.swing.GroupLayout(panelPengeluaran);
        panelPengeluaran.setLayout(panelPengeluaranLayout);
        panelPengeluaranLayout.setHorizontalGroup(
            panelPengeluaranLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelPengeluaranLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelPengeluaranLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(panelPengeluaranLayout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(hapusPengeluaran, javax.swing.GroupLayout.PREFERRED_SIZE, 111, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(panelPengeluaranLayout.createSequentialGroup()
                        .addGroup(panelPengeluaranLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(labelNamaPengeluaran, javax.swing.GroupLayout.PREFERRED_SIZE, 138, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(labelNominalPengeluaran, javax.swing.GroupLayout.PREFERRED_SIZE, 123, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(26, 26, 26)
                        .addGroup(panelPengeluaranLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(tambahPengeluaran, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(inputNominalPengeluaran, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(inputNamaPengeluaran, javax.swing.GroupLayout.Alignment.TRAILING))
                        .addGroup(panelPengeluaranLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(panelPengeluaranLayout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jScrollPane3, javax.swing.GroupLayout.DEFAULT_SIZE, 486, Short.MAX_VALUE))
                            .addGroup(panelPengeluaranLayout.createSequentialGroup()
                                .addGap(61, 61, 61)
                                .addComponent(labelTotalPengeluaran, javax.swing.GroupLayout.PREFERRED_SIZE, 275, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(labelNominalTotalPengeluaran, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))))
                .addContainerGap())
        );

        panelPengeluaranLayout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {labelNamaPengeluaran, labelNominalPengeluaran});

        panelPengeluaranLayout.setVerticalGroup(
            panelPengeluaranLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelPengeluaranLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelPengeluaranLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(labelNamaPengeluaran, javax.swing.GroupLayout.DEFAULT_SIZE, 41, Short.MAX_VALUE)
                    .addComponent(inputNamaPengeluaran)
                    .addComponent(labelTotalPengeluaran, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(labelNominalTotalPengeluaran, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(18, 18, 18)
                .addGroup(panelPengeluaranLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelPengeluaranLayout.createSequentialGroup()
                        .addGroup(panelPengeluaranLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(labelNominalPengeluaran, javax.swing.GroupLayout.DEFAULT_SIZE, 37, Short.MAX_VALUE)
                            .addComponent(inputNominalPengeluaran))
                        .addGap(18, 18, 18)
                        .addComponent(tambahPengeluaran, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 169, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(hapusPengeluaran, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(187, Short.MAX_VALUE))
        );

        panelPengeluaranLayout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {labelNamaPengeluaran, labelNominalPengeluaran});

        jTabbedPane1.addTab("Cash - Out", panelPengeluaran);

        labelSaldoAndaSaatIni.setFont(new java.awt.Font("Calibri", 1, 24)); // NOI18N
        labelSaldoAndaSaatIni.setText("Saldo Anda Saat ini :");

        labelSaldo.setFont(new java.awt.Font("Calibri", 1, 24)); // NOI18N
        labelSaldo.setText("saldo");

        javax.swing.GroupLayout panelReportLayout = new javax.swing.GroupLayout(panelReport);
        panelReport.setLayout(panelReportLayout);
        panelReportLayout.setHorizontalGroup(
            panelReportLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelReportLayout.createSequentialGroup()
                .addGap(40, 40, 40)
                .addComponent(labelSaldoAndaSaatIni, javax.swing.GroupLayout.PREFERRED_SIZE, 213, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(labelSaldo, javax.swing.GroupLayout.PREFERRED_SIZE, 142, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(396, Short.MAX_VALUE))
        );
        panelReportLayout.setVerticalGroup(
            panelReportLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelReportLayout.createSequentialGroup()
                .addGap(39, 39, 39)
                .addGroup(panelReportLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(labelSaldoAndaSaatIni, javax.swing.GroupLayout.PREFERRED_SIZE, 55, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(labelSaldo))
                .addContainerGap(381, Short.MAX_VALUE))
        );

        panelReportLayout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {labelSaldo, labelSaldoAndaSaatIni});

        jTabbedPane1.addTab("Cash Flow Report", panelReport);

        generateListUser.setText("Generate Users");
        generateListUser.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                generateListUserActionPerformed(evt);
            }
        });

        deleteUser.setText("Hapus user terpilih");
        deleteUser.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                deleteUserActionPerformed(evt);
            }
        });

        tableUser.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null},
                {null, null},
                {null, null},
                {null, null},
                {null, null}
            },
            new String [] {
                "id_user", "username"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Integer.class, java.lang.String.class
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }
        });
        jScrollPane2.setViewportView(tableUser);

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGap(25, 25, 25)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(deleteUser)
                    .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(generateListUser, javax.swing.GroupLayout.PREFERRED_SIZE, 157, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 379, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(405, Short.MAX_VALUE))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGap(19, 19, 19)
                .addComponent(generateListUser, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 186, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(deleteUser, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(171, Short.MAX_VALUE))
        );

        jTabbedPane1.addTab("Accounts Manager", jPanel4);

        welcomeLabel.setText("Selamat Datang,");

        usernameLabel.setText("Username");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jTabbedPane1)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(welcomeLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 98, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(usernameLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 187, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(27, 27, 27)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(usernameLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(welcomeLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jTabbedPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 503, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void tambahPemasukanActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tambahPemasukanActionPerformed
        // TODO add your handling code here:
        String nama_pemasukan = inputNamaPemasukan.getText();
        int nominal_pemasukan = Integer.parseInt(inputNominalPemasukan.getText());
        //String user = usernameLabel.getText();

        koneksi.insertPemasukan(nama_pemasukan, nominal_pemasukan);
        inputNamaPemasukan.setText("");
        inputNominalPemasukan.setText("");
        selectPemasukan();
        selectTotalPemasukan();
        selectSaldo();
    }//GEN-LAST:event_tambahPemasukanActionPerformed

    private void generateListUserActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_generateListUserActionPerformed
        // TODO add your handling code here:
        selectUser();
    }//GEN-LAST:event_generateListUserActionPerformed

    private void deleteUserActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_deleteUserActionPerformed
        // TODO add your handling code here:
        int selected_row = tableUser.getSelectedRow();
        String selected_id = tableUser.getModel().getValueAt(selected_row,0).toString();
        
        if(selected_row != -1)
        {
            int selectedOption = JOptionPane.showConfirmDialog(null, "Apakah anda yakin ingin menghapus akun ini?");
            if (selectedOption == JOptionPane.YES_OPTION)
            {
                koneksi.deleteUser(Integer.parseInt(selected_id));
                selectUser();
                JOptionPane.showMessageDialog(null, "Akun berhasil dihapus!");
            }
        }
    }//GEN-LAST:event_deleteUserActionPerformed

    private void formWindowOpened(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowOpened
        // TODO add your handling code here:
        selectUser();
        
        selectPemasukan();
        selectTotalPemasukan();
        selectPengeluaran();
        selectTotalPengeluaran();
        selectSaldo();
    }//GEN-LAST:event_formWindowOpened

    private void hapusPemasukanActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_hapusPemasukanActionPerformed
        // TODO add your handling code here:
        int selected_row = tabelRincianPemasukan.getSelectedRow();
        String selected_id = tabelRincianPemasukan.getModel().getValueAt(selected_row,0).toString();
        
        if(selected_row != -1)
        {
            int selectedOption = JOptionPane.showConfirmDialog(null, "Apakah anda yakin ingin menghapus pemasukan ini?");
            if (selectedOption == JOptionPane.YES_OPTION)
            {
                koneksi.deletePemasukan(Integer.parseInt(selected_id));
                selectPemasukan();
                selectTotalPemasukan();
                selectSaldo();
                JOptionPane.showMessageDialog(null, "Pemasukan berhasil dihapus!");
            }
        }
    }//GEN-LAST:event_hapusPemasukanActionPerformed

    private void tambahPengeluaranActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tambahPengeluaranActionPerformed
        // TODO add your handling code here:
        String nama_pengeluaran = inputNamaPengeluaran.getText();
        int nominal_pengeluaran = Integer.parseInt(inputNominalPengeluaran.getText());
        //String user = usernameLabel.getText();
        
        koneksi.insertPengeluaran(nama_pengeluaran, nominal_pengeluaran);
        inputNamaPengeluaran.setText("");
        inputNominalPengeluaran.setText("");
        selectPengeluaran();
        selectTotalPengeluaran();
        selectSaldo();
    }//GEN-LAST:event_tambahPengeluaranActionPerformed

    private void hapusPengeluaranActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_hapusPengeluaranActionPerformed
        // TODO add your handling code here:
        int selected_row = tabelRincianPengeluaran.getSelectedRow();
        String selected_id = tabelRincianPengeluaran.getModel().getValueAt(selected_row,0).toString();
        
        if(selected_row != -1)
        {
            int selectedOption = JOptionPane.showConfirmDialog(null, "Apakah anda yakin ingin menghapus pengeluaran ini?");
            if (selectedOption == JOptionPane.YES_OPTION)
            {
                koneksi.deletePengeluaran(Integer.parseInt(selected_id));
                selectPengeluaran();
                selectTotalPengeluaran();
                selectSaldo();
                JOptionPane.showMessageDialog(null, "Pengeluaran berhasil dihapus!");
            }
        }
    }//GEN-LAST:event_hapusPengeluaranActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(StudentMoneyManagerGUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(StudentMoneyManagerGUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(StudentMoneyManagerGUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(StudentMoneyManagerGUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new StudentMoneyManagerGUI().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton deleteUser;
    private javax.swing.JButton generateListUser;
    private javax.swing.JButton hapusPemasukan;
    private javax.swing.JButton hapusPengeluaran;
    private javax.swing.JTextField inputNamaPemasukan;
    private javax.swing.JTextField inputNamaPengeluaran;
    private javax.swing.JTextField inputNominalPemasukan;
    private javax.swing.JTextField inputNominalPengeluaran;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JLabel labelNamaPemasukan;
    private javax.swing.JLabel labelNamaPengeluaran;
    private javax.swing.JLabel labelNominalPemasukan;
    private javax.swing.JLabel labelNominalPengeluaran;
    private javax.swing.JLabel labelNominalTotalPemasukan;
    private javax.swing.JLabel labelNominalTotalPengeluaran;
    private javax.swing.JLabel labelSaldo;
    private javax.swing.JLabel labelSaldoAndaSaatIni;
    private javax.swing.JLabel labelTotalPemasukan;
    private javax.swing.JLabel labelTotalPengeluaran;
    private javax.swing.JPanel panelPemasukan;
    private javax.swing.JPanel panelPengeluaran;
    private javax.swing.JPanel panelReport;
    public javax.swing.JTable tabelRincianPemasukan;
    private javax.swing.JTable tabelRincianPengeluaran;
    private javax.swing.JTable tableUser;
    private javax.swing.JButton tambahPemasukan;
    private javax.swing.JButton tambahPengeluaran;
    private javax.swing.JLabel usernameLabel;
    private javax.swing.JLabel welcomeLabel;
    // End of variables declaration//GEN-END:variables
}