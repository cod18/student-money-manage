package studentmoneymanager;

import java.sql.*;

/**
 *
 * @author KrismaAditya
 */
public class sql {
    
    //fungsi bikin database baru
    public static void createNewDatabase(String fileName)
    {
        String url = "jdbc:sqlite:C:/Users/KrismaAditya/My Documents/NetBeansProjects/" + fileName;
       
        try (Connection conn = DriverManager.getConnection(url))
        {
            if (conn != null)
            {
                DatabaseMetaData meta = conn.getMetaData();
                System.out.println("The driver name is " + meta.getDriverName());
                System.out.println("A new database has been created.");
            }
        }
        catch (SQLException e)
        {
            System.out.println(e.getMessage());
        }
    }
    
    //fungsi bikin table baru di database yang udah dibikin
    public static void createNewTable()
    {
        String url = "jdbc:sqlite:C:/Users/KrismaAditya/My Documents/NetBeansProjects/studentmoneymanager.db";
        
        String sql_user = "CREATE TABLE IF NOT EXISTS user (\n"
                + " id_user integer PRIMARY KEY,\n"
                + " username text NOT NULL\n"
                + ");";
        
        String sql_pemasukan = "CREATE TABLE IF NOT EXISTS pemasukan (\n"
                + "id_pemasukan integer PRIMARY KEY, \n"
                + "nama_pemasukan text NOT NULL, \n"
                + "nominal_pemasukan integer NOT NULL, \n"
                + "id_user integer references user(id_user)\n"
                + ");";
        
        String sql_pengeluaran = "CREATE TABLE IF NOT EXISTS pengeluaran (\n"
                + "id_pengeluaran integer PRIMARY KEY, \n"
                + "nama_pengeluaran text NOT NULL, \n"
                + "nominal_pengeluaran integer NOT NULL, \n"
                + "id_user integer references user(id_user)\n"
                + ");";
        
        try (Connection conn = DriverManager.getConnection(url);
                Statement stmt = conn.createStatement())
        {
            stmt.execute(sql_user);
            stmt.execute(sql_pemasukan);
            stmt.execute(sql_pengeluaran);
            System.out.println("A new table has been created succesfully");
        }
        catch (SQLException e)
        {
            System.out.println(e.getMessage());
        }
    }
    
    //fungsi buat konekin ke databasenya
    public Connection connect()
    {
        Connection conn = null;
        
        try
        {
            String url = "jdbc:sqlite:C:/Users/KrismaAditya/My Documents/NetBeansProjects/studentmoneymanager.db";
            conn = DriverManager.getConnection(url);
            
            System.out.println("Connection to SQLite BERHASIL BRO!");
        }
        catch(SQLException ex)
        {
            System.out.println(ex.getMessage());
        }
        return conn;
    }
    
    //Fungsi select semua user
    public void selectAll()
    {
        String sql = "SELECT * FROM user";
        
        try (Connection conn = this.connect();
                Statement stmt = conn.createStatement();
                ResultSet rs = stmt.executeQuery(sql))
        {
            while(rs.next())
            {
                //System.out.println(rs.getInt("id_user")+rs.getString("username"));
                ResultSetMetaData md = rs.getMetaData();
                int columns = md.getColumnCount();
                
                /*if(columns == 0)
                {
                    //kalau kolom yang ditemukan 0, masuk ke welcome page
                    welcomepage gui = new welcomepage();
                    gui.setVisible(true);
                }
                else if(columns >= 1)
                {
                    StudentMoneyManagerGUI gui = new StudentMoneyManagerGUI();
                    gui.setVisible(true);
                }*/
                
                Object[] row = new Object[columns];
                for (int i = 1; i <= columns; i++)
                {  
                    row[i - 1] = rs.getObject(i);
                    System.out.println(rs.getObject(i));
                }
            }
        }
        catch(SQLException e)
        {
            System.out.println(e.getMessage());
        }
    }
    
    //Fungsi insert user
    public void insert(String username)
    {
       String sql = "INSERT INTO user(username) VALUES(?)";
       
       try(Connection conn = connect();
           PreparedStatement pstmt = conn.prepareStatement(sql))
       {
           pstmt.setString(1, username);
           pstmt.executeUpdate();
       }
       catch (SQLException e)
       {
           System.out.println(e.getMessage());
       }
    }
    
    //fungsi menghapus user satu persatu berdasarkan id_user yang diklik
    public void deleteUser(int id_user)
    {
        String sql = "DELETE FROM user WHERE id_user = ?";
        
        try (Connection conn = connect();
                PreparedStatement pstmt = conn.prepareStatement(sql))
        {
            pstmt.setInt(1, id_user);
            pstmt.executeUpdate();
        }
        catch (SQLException e)
        {
            System.out.println(e.getMessage());
        }
    }
    
    //Fungsi Insert Pemasukan
    public void insertPemasukan(String nama_pemasukan, int nominal_pemasukan)
    {
        String sql = "INSERT INTO pemasukan(nama_pemasukan, nominal_pemasukan) VALUES(?,?)";
        try(Connection conn = connect();
               PreparedStatement pstmt = conn.prepareStatement(sql))
       {
           pstmt.setString(1, nama_pemasukan);
           pstmt.setInt(2, nominal_pemasukan);
           pstmt.executeUpdate();
       }
       catch (SQLException e)
       {
           System.out.println(e.getMessage());
       }
    }
    
    //Fungsi hapus pemasukan
    public void deletePemasukan(int id_pemasukan)
    {
        String sql = "DELETE FROM pemasukan WHERE id_pemasukan = ?";
        
        try (Connection conn = connect();
                PreparedStatement pstmt = conn.prepareStatement(sql))
        {
            pstmt.setInt(1, id_pemasukan);
            pstmt.executeUpdate();
        }
        catch (SQLException e)
        {
            System.out.println(e.getMessage());
        }
    }
    
    //Fungsi Insert pengeluaran
    public void insertPengeluaran(String nama_pengeluaran, int nominal_pengeluaran)
    {
        String sql = "INSERT INTO pengeluaran(nama_pengeluaran, nominal_pengeluaran) VALUES(?,?)";
        try(Connection conn = connect();
               PreparedStatement pstmt = conn.prepareStatement(sql))
       {
           pstmt.setString(1, nama_pengeluaran);
           pstmt.setInt(2, nominal_pengeluaran);
           pstmt.executeUpdate();
       }
       catch (SQLException e)
       {
           System.out.println(e.getMessage());
       }
    }
    
    //Fungsi hapus pengeluaran
    public void deletePengeluaran(int id_pemasukan)
    {
        String sql = "DELETE FROM pengeluaran WHERE id_pengeluaran = ?";
        
        try (Connection conn = connect();
                PreparedStatement pstmt = conn.prepareStatement(sql))
        {
            pstmt.setInt(1, id_pemasukan);
            pstmt.executeUpdate();
        }
        catch (SQLException e)
        {
            System.out.println(e.getMessage());
        }
    }   
}